/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intintab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/22 14:24:59 by flime             #+#    #+#             */
/*   Updated: 2015/02/22 14:25:32 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "../includes/libft.h"

/*
**	Search an integer in a tab of integers return the key or -1 if not found
*/

int	ft_intintab(int n, int *tab, size_t size)
{
	size_t	i;

	i = 0;
	while (i < size)
	{
		if (tab[i] == n)
			return (i);
		i++;
	}
	return (-1);
}
