/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 07:30:56 by flime             #+#    #+#             */
/*   Updated: 2013/12/10 07:30:57 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int			ft_iswp(int c)
{
	return (c == ' ' || c == '\n' || c == '\t');
}

char				*ft_strtrim(char const *s)
{
	unsigned int	start;
	size_t			len;

	start = 0;
	while (ft_iswp(s[start]))
		start++;
	len = ft_strlen(s) - 1;
	while (len && ft_iswp(s[len]))
		len--;
	if (len < start)
		return (ft_strdup(""));
	return (ft_strsub(s, start, len - (size_t)start + 1));
}
