/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putkvendl.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/22 14:30:05 by flime             #+#    #+#             */
/*   Updated: 2015/02/22 14:30:06 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
**	Print array key and value
*/

void	ft_putkvendl(char **str, int key)
{
	ft_putstr(" [");
	ft_putnbr(key);
	ft_putstr("] => ");
	ft_putendl(str[key]);
}
