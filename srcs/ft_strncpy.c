/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strncpy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 18:12:10 by flime             #+#    #+#             */
/*   Updated: 2013/12/01 23:04:59 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char		*ft_strncpy(char *s1, const char *s2, size_t n)
{
	char	*dst;

	dst = s1;
	while (n)
	{
		*dst = *s2;
		dst++;
		if (*s2)
		{
			s2++;
		}
		n--;
	}
	return (s1);
}
