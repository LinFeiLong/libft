/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 07:08:54 by flime             #+#    #+#             */
/*   Updated: 2013/12/10 07:08:58 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int			ft_memcmp(const void *s1, const void *s2, size_t n)
{
	int		c;

	while (n)
	{
		c = (int)(*((unsigned char*)s1) - *((unsigned char*)s2));
		s1 = (void*)((char*)s1 + 1);
		s2 = (void*)((char*)s2 + 1);
		if (c)
		{
			return (c);
		}
		n--;
	}
	return (0);
}
