/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 21:58:44 by flime             #+#    #+#             */
/*   Updated: 2014/04/26 20:47:47 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (s2[i] == '\0')
		return ((char *)s1);
	while (s1[i] && i + ft_strlen(s2) <= n)
	{
		if (ft_strncmp(&s1[i], s2, ft_strlen(s2)) == 0)
			return (&((char *)s1)[i]);
		i++;
	}
	return (NULL);
}
