/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/10 07:27:28 by flime             #+#    #+#             */
/*   Updated: 2013/12/10 07:27:29 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../includes/libft.h"

char		*ft_strdup(const char *s1)
{
	char	*str;

	str = (char*)(malloc(ft_strlen(s1) + 1));
	if (str)
	{
		ft_strcpy(str, s1);
	}
	return (str);
}
