/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strisint.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flime <flime@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/22 14:27:46 by flime             #+#    #+#             */
/*   Updated: 2015/02/22 14:27:46 by flime            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

/*
**	Search if a string is a valid INT
*/

int		ft_strisint(char *str)
{
	int		len;
	char	*intcmp;
	int		i;
	int		j;

	intcmp = str[0] == '-' ? "-2147483648" : "2147483647";
	if (str[0] == '+')
		intcmp = "+2147483647";
	j = (str[0] == '-' || str[0] == '+') ? 1 : 0;
	i = j;
	while (str[i] == '0')
		i++;
	len = ft_strlen(str) - i + j;
	if ((len <= 9) || (len <= 10 && (str[0] == '+' || str[0] == '-')))
		return (1);
	else if (len == 10 || (len == 11 && (str[0] == '+' || str[0] == '-')))
	{
		while (str[i])
		{
			if (str[i++] > intcmp[j++])
				return (0);
		}
		return (1);
	}
	return (0);
}
